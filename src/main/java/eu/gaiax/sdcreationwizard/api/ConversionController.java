package eu.gaiax.sdcreationwizard.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gaiax.sdcreationwizard.api.dto.CategorizedShapes;
import eu.gaiax.sdcreationwizard.api.dto.Formaliser;
import eu.gaiax.sdcreationwizard.api.dto.ShaclModel;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Main entry point for REST calls. Provides access to pre-defined files as well as a Turtle to JSON preprocessor for the frontend.
 */
@RestController
public class ConversionController {
    private static final Logger logger = LoggerFactory.getLogger(ConversionController.class);

    private static final CategorizedShapes CATEGORIZED_SHAPES;

    static {
        String[] extensions = {"ttl"};

        Collection<File> files = FileUtils.listFiles(new File(ConversionService.SHAPES_DIR), extensions, true);
        Formaliser formaliser = new Formaliser(files);
        // triggered only if necessary i.e when new files have been loaded
        formaliser.formalise();
        // reload files to be correct format
        files = FileUtils.listFiles(new File(ConversionService.SHAPES_DIR), extensions, true);

        File[] shaclFiles = files.toArray(new File[files.size()]);

        boolean processedExists = !new File(ConversionService.PROCESSED_DIR).mkdirs();

        // Delete cached processed files if needed
        logger.info("Cache dir: {}", new File(ConversionService.PROCESSED_DIR).getPath());

        File[] cachedFiles = new File(ConversionService.PROCESSED_DIR).listFiles();

        if (processedExists && cachedFiles != null && cachedFiles.length > 0) {
            int successes = 0;
            int fails = 0;

            for (File cachedFile : cachedFiles) {
                if (cachedFile.isFile()) {
                    try {
                        Files.delete(cachedFile.toPath());
                        successes++;
                    } catch (IOException e) {
                        logger.warn("Failed to delete cache file", e);
                        fails++;
                    }
                }
            }

            logger.info("Removed {} cached files, failed to remove {} cached files from {}.", successes, fails, ConversionService.SHAPES_DIR);
        } else {
            logger.info("No cached files to clean.");
        }

        Map<String, Map<String, List<String>>> ecosystemMap = new HashMap<>();

        if (shaclFiles != null) {
            logger.info("Found up to {} SHACL files to process.", shaclFiles.length);
            for (File shaclFile : shaclFiles) {
                if (shaclFile.isFile()) {
                    try {

                        logger.info("Converting file: {}", shaclFile.getPath());

                        // Convert SHACL to JSONS
                        ShaclModel shaclModel = ConversionService.convertToJson(new FileInputStream(shaclFile));
                        String filename = shaclFile.getName().substring(0, shaclFile.getName().lastIndexOf(".")) + ".json";

                        // Add converted shape to its category
                        categorizeShaclModel(shaclModel, filename, shaclFile, ecosystemMap);

                        // Save to file as cache
                        logger.debug("Saving file to JSON: {}", filename);
                        new ObjectMapper().writeValue(new File(ConversionService.PROCESSED_DIR, filename), shaclModel);
                    } catch (IOException e) {
                        logger.warn("Failed to read or convert file, skipping.", e);
                    }
                }
            }
        } else {
            logger.warn("Found no SHACL files to process.");
        }

        CATEGORIZED_SHAPES = new CategorizedShapes(ecosystemMap);


    }

    private static void categorizeShaclModel(ShaclModel shaclModel, String filename, File shaclFile, Map<String, Map<String, List<String>>> ecosystemMap) {
        /**
         shaclModel.getShapes().stream()
         // Read prefix
         .map(VicShape::getTargetClassPrefix)

         // Determine user-friendly category
         .map(prefix -> {
         String res = PREFIX_TO_CATEGORY_MAP.get(prefix);
         return (res != null) ? res : DEFAULT_CATEGORY;
         })

         // Add to all required categories (note that there might be more than one)
         .forEach(category -> {
         if (!categoryMap.containsKey(category)) {
         categoryMap.put(category, new ArrayList<>());
         }

         categoryMap.get(category).add(filename);
         });
         */


        String ecosystem = shaclFile.getParentFile().getParent();
        String category = shaclFile.getParent();

        if (!ecosystemMap.containsKey(ecosystem)) {
            ecosystemMap.put(ecosystem, new HashMap<>());
        }

        Map<String, List<String>> categoryMap = ecosystemMap.get(ecosystem);

        if (!categoryMap.containsKey(category)) {
            categoryMap.put(category, new ArrayList<>());
        }

        categoryMap.get(category).add(filename);

    }

    /**
     * Returns all available predefined SHACL files. All *.json files same directory will be considered.
     *
     * @return a set of file names of all available SHACL files
     */
    @GetMapping(value = "/getAvailableShapes")
    @CrossOrigin(origins = "*")
    public Set<String> getAvailableShapes() {
        Set<String> availableShapes = new HashSet<>();

        try (var stream = Files.list(Paths.get(ConversionService.PROCESSED_DIR))) {
            availableShapes = stream
                    .filter(file -> !Files.isDirectory(file))
                    .map(Path::getFileName)
                    .map(Path::toString)
                    .filter(fileName -> fileName.endsWith(".json"))
                    .collect(Collectors.toSet());
        } catch (IOException e) {
            e.printStackTrace();
            // We intentionally drop any exception so that the list just stays empty.
        }

        return availableShapes;
    }

    /**
     * Converts a given SHACL file to a specific JSON representation
     *
     * @param shaclFile a multipart SHACL file with *.ttl extension
     * @return a JSON serialization of the processed content to be used in VIC
     */
    @PostMapping(value = "/convertFile", produces = "application/json")
    @CrossOrigin(origins = "*")
    @ResponseBody
    public ResponseEntity<Object> convertShacl(@RequestParam("file") MultipartFile shaclFile) {
        logger.info("Converting SHACL file: {}", shaclFile.getOriginalFilename());
        ShaclModel shaclModel;
        try {
            shaclModel = ConversionService.convertToJson(shaclFile);
        } catch (IOException | IllegalArgumentException e) {
            return new ResponseEntity<>("Shacl conversion failed <-> Bad Input", HttpStatus.BAD_REQUEST);
        }
        if (shaclModel.getPrefixList().isEmpty() && shaclModel.getShapes().isEmpty())
            return new ResponseEntity<>(shaclModel, HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(shaclModel, HttpStatus.OK);
    }


    /**
     * To be used after /getAvailableShapes. Returns the content of a predefined JSON file.
     *
     * @param name the file name to return. Can be chosen from /getAvailableShapes' response.
     * @return the full content of the respective JSON file.
     */
    @GetMapping(value = "/getJSON")
    @CrossOrigin(origins = "*")
    @ResponseBody
    public ResponseEntity<String> getJSON(String name) {
        if (name == null) {
            return new ResponseEntity<>("Input name no specified", HttpStatus.BAD_REQUEST);
        }

        String s;
        try {
            s = Files.readString(new File(ConversionService.PROCESSED_DIR, name).toPath());
        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
            return new ResponseEntity<>("File not found", HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(s, HttpStatus.OK);
    }

    /**
     * @GetMapping(value = "/getAvailableShapesCategorized")
     * @CrossOrigin(origins = "*")
     * public ResponseEntity<CategorizedShapes> getAvailableShapesCategorized() {
     * return new ResponseEntity<>(CATEGORIZED_SHAPES, HttpStatus.OK);
     * }
     */
    @GetMapping(value = "/getAvailableShapesCategorized")
    @CrossOrigin(origins = "*")
    @ResponseBody
    public ResponseEntity<Map<String, List<String>>> getAvailableShapesCategorized(String ecoSystem) {

        if (CATEGORIZED_SHAPES.getShapes().containsKey(ecoSystem)) {
            Map<String, List<String>> eco_system_map = CATEGORIZED_SHAPES.getEcosystem(ecoSystem);
            return new ResponseEntity<>(eco_system_map, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(CATEGORIZED_SHAPES.getShapes().get("trusted-cloud"), HttpStatus.BAD_REQUEST);
        }
    }


    // Todo
    @RequestMapping(path = "/getSearchQuery/{ecoSystem}/{query}", method = RequestMethod.GET)
    // http://localhost:8080/api/v1/mno/objectKey/1/Saif
    @CrossOrigin(origins = "*")
    @ResponseBody
    public ResponseEntity<Map<String, List<String>>> getSearchQuery(@PathVariable String ecoSystem, @PathVariable String query) {

        if (CATEGORIZED_SHAPES.getShapes().containsKey(ecoSystem)) {

            Map<String, List<String>> eco_system = (CATEGORIZED_SHAPES.getEcosystem(ecoSystem));
            Map<String, List<String>> selection = new HashMap<>();

            System.out.println(eco_system.keySet());
            for (String key : eco_system.keySet()) {
                List<String> list = eco_system.get(key);
                System.out.println(list);
                for (String itm : list) {
                    if (itm.toLowerCase().contains(query.toLowerCase())) {

                        List<String> l_tmp = selection.get(key);
                        if (!selection.containsKey(key)) {
                            selection.put(key, new ArrayList<>());
                        }

                        //selection.get(key).add()

                        System.out.println(itm);
                    }

                }

            }

            return new ResponseEntity<>(selection, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }


}
